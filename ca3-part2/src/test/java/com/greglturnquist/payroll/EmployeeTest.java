package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    @DisplayName("Test getter function for First Name")
    void testGetFirstNameSuccess() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");
        String expected = "Frodo";

        //Act
        String real = oneEmployee.getFirstName();

        //Assert
        assertEquals(expected, real);

    }


    @Test
    @DisplayName("Test setter function for firstName-  valid Name input")
    void testSetFirstNameValidInput() {
        //Arrange
        Employee oneEmployee = new Employee("Frod", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");
        String expected = "Frodo";

        //Act
        oneEmployee.setFirstName("Frodo");
        String real = oneEmployee.getFirstName();

        //Assert
        assertEquals(expected, real);

    }

    @Test
    @DisplayName("Test setter function for First Name - null case")
    void testSetFirstNameNull() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");

        try {
            //Act
            oneEmployee.setFirstName(null);
            fail();
        }
        //Assert
        catch (IllegalArgumentException invalidFirstName) {
            assertEquals("The First Name can´t be null or empty!", invalidFirstName.getMessage());
        }

    }

    @Test
    @DisplayName("Test setter function for First Name - Empty case")
    void testSetFirstNameEmpty() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Burglar", "frodo@hotmail.com");

        try {
            //Act
            oneEmployee.setFirstName("");
            fail();
        }
        //Assert
        catch (IllegalArgumentException invalidFirstName) {
            assertEquals("The First Name can´t be null or empty!", invalidFirstName.getMessage());
        }

    }


    @Test
    @DisplayName("Test getter function for Last Name")
    void getLastName() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Burglar", "frodo@hotmail.com");
        String expected = "Baggins";

        //Act
        String real = oneEmployee.getLastName();

        //Assert
        assertEquals(expected, real);

    }

    @Test
    @DisplayName("Test setter function for valid Last Name - Valid Case")
    void testSetLastNameValidInput() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Bag", "ring bearer", "burglar", "frodo@hotmail.com");
        String expected = "Baggins";

        //Act
        oneEmployee.setLastName("Baggins");
        String real = oneEmployee.getLastName();

        //Assert
        assertEquals(expected, real);

    }

    @Test
    @DisplayName("Test setter function for Last Name - Null Case")
    void testSetLastNameNull() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");

        try {
            //Act
            oneEmployee.setLastName(null);
            fail();
        }
        //Assert
        catch (IllegalArgumentException invalidLastName) {
            assertEquals("The Last Name can´t be null or empty!", invalidLastName.getMessage());
        }

    }

    @Test
    @DisplayName("Test setter function for Last Name - Empty Case")
    void testSetLastNameEmpty() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");

        try {
            //Act
            oneEmployee.setLastName("");
        }
        //Assert
        catch (IllegalArgumentException invalidLastName) {
            assertEquals("The Last Name can´t be null or empty!", invalidLastName.getMessage());
        }

    }


    @Test
    @DisplayName("Test getter function for Description")
    void getDescription() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");
        String expected = "ring bearer";

        //Act
        String real = oneEmployee.getDescription();

        //Assert
        assertEquals(expected, real);

    }


    @Test
    @DisplayName("Test setter function for Description - valid case")
    void testSetDescriptionValidInput() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo1@hotmail.com");
        String expected = "ring bearer";

        //Act
        oneEmployee.setDescription("ring bearer");
        String real = oneEmployee.getDescription();

        //Assert
        assertEquals(expected, real);

    }

    @Test
    @DisplayName("Test setter function for Description - null case")
    void testDescriptionNull() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");
        try {
            //Act
            oneEmployee.setDescription(null);
        }
        //Assert
        catch (IllegalArgumentException invalidDescription) {
            assertEquals("The Description can´t be null or empty!", invalidDescription.getMessage());
        }
    }

    @Test
    @DisplayName("Test setter function for Description - Empty Case")
    void testDescription_Empty() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");

        try {
            //Act
            oneEmployee.setDescription("");
        }
        //Assert
        catch (IllegalArgumentException invalidDescription) {
            assertEquals("The Description can´t be null or empty!", invalidDescription.getMessage());
        }

    }


    @Test
    @DisplayName("Test getter function for Job Title")
    void geJobTitle() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");
        String expected = "burglar";

        //Act
        String real = oneEmployee.getJobTitle();

        //Assert
        assertEquals(expected, real);

    }


    @Test
    @DisplayName("Test setter function for Job Title - valid case")
    void testSetJobTitleValidInput() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");
        String expected = "teacher";

        //Act
        oneEmployee.setJobTitle("teacher");
        String real = oneEmployee.getJobTitle();

        //Assert
        assertEquals(expected, real);

    }

    @Test
    @DisplayName("Test setter function for Job Title - null Case")
    void testSetJobTitleNull() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");

        try {
            //Act
            oneEmployee.setJobTitle(null);
        }
        //Assert
        catch (IllegalArgumentException invalidJobTile) {
            assertEquals("The Job Title can´t be null or empty!", invalidJobTile.getMessage());
        }
    }

    @Test
    @DisplayName("Test setter function for Job Title - empty case")
    void testJobTitleEmpty() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");

        try {
            //Act
            oneEmployee.setJobTitle("");
        }
        //Assert
        catch (IllegalArgumentException invalidJobTitle) {
            assertEquals("The Job Title can´t be null or empty!", invalidJobTitle.getMessage());
        }

    }

    @Test
    @DisplayName("Test email getter function")
    void getEmail() {
        //Arrange
        Employee newEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo.bagins@gmail.com");

        //Act
        String expectedEmail = "frodo.bagins@gmail.com";

        //Assert
        assertEquals(expectedEmail, newEmployee.getEmail());
    }

    @Test
    @DisplayName("Test email setter function - update email")
    void setEmail() {
        //Arrange
        Employee newEmployee = new Employee("Marta", "Cardoso", "DEVOPS", "Intern", "marta@ourcompany.com");
        String expectedEmail = "marta.cardoso@ourcompany.com";
        //Act

        newEmployee.setEmail("marta.cardoso@ourcompany.com");
        String actualEmail = newEmployee.getEmail();

        //Assert
        assertEquals(expectedEmail, actualEmail);
    }

    @Test
    @DisplayName("Test email setter function - null case")
    void setEmailNullCase() {
        //Arrange
        Employee newEmployee = new Employee("Maria", "Sousa", "Devops", "Intern", "mariasousa@ourcompany.pt");

        try {
            //Act
            newEmployee.setEmail(null);
            fail();
        } catch (IllegalArgumentException output) {
            //Assert
            assertEquals("The E-mail can´t be null or empty!", output.getMessage());
        }
    }

    @Test
    @DisplayName("Test email setter function - empty case")
    void setEmailEmptyCase() {
        //Arrange
        Employee newEmployee = new Employee("João", "Alves", "IT", "Developer", "joao.alves@ourcompany.pt");

        try {
            //Act
            newEmployee.setEmail("");
            fail();
        } catch (IllegalArgumentException output) {
            //Assert
            assertEquals("The E-mail can´t be null or empty!", output.getMessage());
        }
    }

    @Test
    @DisplayName("Test email setter function - No at sign")
    public void setEmailNoAtSign() {

        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");

        //Act:
        try {
            oneEmployee.setEmail("frodohotmail.com");
        }

        //Assert
        catch (IllegalArgumentException invalidEmail) {
            assertEquals("The E-mail is not valid.", invalidEmail.getMessage());
        }

    }

    @Test
    @DisplayName("Test email setter function - No Dot")
    public void invalidEmailNoDot() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "burglar", "frodo@hotmail.com");

        //Act:
        try {
            oneEmployee.setEmail("frodo@hotmailcom");
        }

        //Assert
        catch (IllegalArgumentException invalidEmail) {
            assertEquals("The E-mail is not valid.", invalidEmail.getMessage());
        }
    }
}