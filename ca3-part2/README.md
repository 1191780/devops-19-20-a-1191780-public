# Class Assignment 2 Report - Part 2 - Build Tools with Gradle

## Brief Description

Build tools are programs that automate the process of creation of an executable application from source code. Building process includes activities like compiling, linking, and packaging the code into an executable form.
The present report is a brief introduction to [Gradle](https://docs.gradle.org/current/userguide/what_is_gradle.html) - an open-source build tool designed to automate the building process of software  - using the command line with an IDE as support. The goal of Part 2 of this assignment is to convert the basic version (i.e., "basic" folder) of the [Tutorial application](https://spring.io/guides/tutorials/react-and-spring-data-rest/) to Gradle, thus the present report aims to illustrate, step-by-step, the implementation of this adaptation.

You should have finished the Class Assignment 2 Part 1! The following subjects covered in the previous Class Assignment - ca2 - Part2 -, should be known:

* Building an application using Gradle from the command line;
* Set up build automation in Gradle;
* Writing tasks of type JavaExe, Copy and Zip in build.gradle;
* Managing project's dependencies.

## Prerequisites

Properly [install Gradle](https://gradle.org/install/) in your system, if you do not use the [gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) in the project, and add the *C:\gradle\bin directory* to the PATH system variable. Verify that you have, at least, [Java JDK version 8  ](https://www.oracle.com/java/technologies/javase-downloads.html) and the JAVA_HOME environmental variable pointing to that version. At last, you should have installed Apache Log4J 2 and the M2_Home environmental variable defined. Finally, make sure you have available on your computer a git command line package and a IDE supporting Git and Gradle.

**Attention**: In this report it was used the **[Cmder](https://cmder.net/)**, a portable console emulator for Windows, and **[IntelliJ IDEA](https://www.jetbrains.com/idea/download/?gclid=EAIaIQobChMI1dzQkMOX6AIVV4jVCh2FNQIwEAAYASABEgIrJvD_BwE#section=windows)**.

## 1. Analysis, Design and Implementation

Please, don't forget to make issues for the following tasks and assign them to your commits!

### Create a new branch tut-basic-gradle

In your repository create a new branch called **tut-basic-gradle**. You should use this branch for this part of the assignment. Use the following git command to create the branch:

```sh
$git checkout -b tut-basic-gradle
```

### Create a folder for part 2 of Class Assignment 2 

Go to Ca2 directory and **create a folder for part 2** using the command **mkdir**:
```sh
$mkdir part2
```

### Start a new Spring Project

As instructed in the readme file of the tutorial, use https://start.spring.io to start a new gradle spring project with the following dependencies:

* Rest Repositories.

* Thymeleaf.

* JPA.

* H2.

It generates a zip file with the Gradle project. Extract the generated zip file inside the folder "CA2/Part2/" of your repository. **You now have an "empty" spring application that can be built using gradle!**


If you run the following command you can check the available tasks of Gradle:

```sh
$gradlew tasks
```

#### Delete 'src' folder from the Spring Project

Delete the existing **src folder** as you will use the code from the tut basic tutorial that you've been working in the last assignments! You can use the **rm** command with the **-r** option to delete the folder and the other files or directories contained in the source folder:

```sh
$rm -r src
```

### Add 'src' folder from tut-basic-react to the Spring Project

Copy the **src folder** and all its subfolders from the basic folder of the tutorial into the part2 folder.
Copy also the files **webpack.config.js** and **package.json** as this files are necessary for the web frontend configuration.

Then, check if the *src / main / resources / static / folder* has a build folder, if it does it should be deleted, since this folder should be generated from the JavaScript by the tool webpack.


### Experiment the application
You can now experiment with the application by using the:

```sh
$gradlew bootRun.
```

Notice that the web page http://localhost:8080 is empty! This is because gradle is missing the plugin for dealing with the frontend code!

### Frontend configurations
Firstly, to gradle can manage the frontend, add the following line to the plugins block in build.gradle:

```sh
$id "org.siouan.frontend" version "1.4.1"
```

Secondly, add also the following code in build.gradle to configure the previous plugin:

```
frontend {
  nodeVersion = "12.13.1" // attribute that points to the version of the Node.js
  assembleScript = "run webpack" // Task performed in the assembler when building the application. Webpack takes the JavaScript from React and compiles it.
}
```
Finally, update the scripts section/object in package.json to configure the execution of webpack (second line inside "scripts"):

```sh
"scripts": {
"watch": "webpack --watch -d",
"webpack": "webpack" // new line
},
```

###  Build
You can now build the project. The tasks related to the frontend should be executed and the frontend should be generated:

```sh
$gradlew build
```

A *'BUILD SUCCESSFUL in 55s, 8 actionable tasks: 8 executed'* should appear on your terminal!

### Execute the application
Now that the frontend is set up and the build was successful, you can run the application by running the command:

```sh
$gradlew bootRun
```

The http://localhost:8080 webpage should no longer be empty and you can now try it using **curl.** to test the server!

### Add a task to gradle to copy the generated jar

Add a task to gradle of the [type Copy](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.Copy.html) to copy the generated '.jar' to a folder named "dist" located at the project root folder level.

Firstly, create the folder named "dist" using the **mkdir** command:

```sh
$mkdir dist
```

Then, add the following task in the build.gradle:

```sh
task copyJar(type: Copy) {
	group = "Devops"
	description = "Copies the generated jar to dist folder"

	from 'build/libs/'
	into 'dist'
}
```

The following table summarizes the properties and methods used in this task:

| Method | Description |
| --- | --- |
|from(sourcePaths) | Specifies source files or directories for a copy|
| into(destDir) |S pecifies the destination directory for a copy. The destination is evaluated as per Project.file(java.lang.Object). |

*Source: https://docs.gradle.org/current/dsl/org.gradle.api.tasks.Copy.html*

Then, verify if the previous task was added to the task list, it should appear on your terminal in DevOps task set, using the following command:

```sh
$gradlew tasks --all.
```

Finally, execute the **copyJar** task from the project's root directory:

```sh
$gradlew copyJar
```

After running the task, change the directory to path\to\my\project\ca2\part1\dist with **cd** command and list the files with **ls -la** command - you will see that the jar file copied!

**If the gradlew build was successful, commit the changes to your remote repository.**

### Add a task to gradle to delete all the files generated by webpack

Add a task of [delete type](https://docs.gradle.org/current/dsl/org.gradle.api.tasks.Delete.html) gradle to delete all the files generated by webpack located at src/main/resources/static/built/ in the build.grade:

```
task deleteWebpackFiles(type: Delete) {
	group = "Devops"
	description = "Delete all the files generated by webpack"
	delete 'src/main/resources/static/built'
}
```
| Properties\Methods | Description |
| --- | --- |
| delete| The set of files which will be deleted by this task. |

*source: https://docs.gradle.org/current/dsl/org.gradle.api.tasks.Delete.html*

This new task should be executed automatically by gradle before the task clean, so it�s necessary to set the 'clean'  task to depend on the executing of the *deleteWebpackFiles*:

```
clean.dependsOn deleteWebpackFiles
```

Then, verify if the previous task was added to the task list, it should appear on your terminal in DevOps task set, using the following command:

```sh
$gradlew tasks --all.
```

Finally, execute the **clean**, since it depends on deleteWebpackFiles task from the project's root directory:

```sh
$gradlew clean
```

After running the task, change the directory to src/main/resources/static with **cd** command and list the files with **ls** command - you will that the built folder was deleted!

**Build the project and if it is successful, commit the changes to your remote repository:**

```sh
$git add .
$git commit -m "message"
$git push -u origin --all
```

### Merge tut-basic-gradle with master

The tut-basic-gradle branch should be merged with the master. You can merge using the following command:

```sh
$git checkout master
$git merge tut-basic-gradle
$git add .
$git push -u origin --all
```

## Summary
At this point, you should be able to:
* Start a Gradle Project with Spring Boot!

## Credits and Sources
https://docs.gradle.org/current/userguide
